import { Box } from '@mui/material'
import { Wrapper } from '@googlemaps/react-wrapper'
import Map3D from './Map3D'
// import MapEmbed from './MapEmbed'

const Map = () => {
  return (
    <>
      <Box
        sx={
          theme => ({
            [theme.breakpoints.down('md')]: {
              display: 'none',
            }
          })
        }
      >
        <Wrapper
          apiKey='AIzaSyAv1V90s2Ho36V0hsq5yI0zR4gy2f5ZpRE'
          version='beta'
        >
          <Map3D />
        </Wrapper>
      </Box >
      <Box
        sx={
          theme => ({
            [theme.breakpoints.up('md')]: {
              display: 'none',
            }
          })
        }
      >
        {/* <MapEmbed /> */}
      </Box>
    </>
  )
}

export default Map