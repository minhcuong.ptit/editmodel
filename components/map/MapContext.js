import { createContext } from 'react'

const MapContext = createContext()

const MapProvider = ({ tower, children, configure }) => {
  return (
    <MapContext.Provider
      value={{ tower, configure}}
    >
      {children}
    </MapContext.Provider>
  )
}

export { MapContext, MapProvider }