import ReactDOM from 'react-dom'
import ChevronLeftOutlinedIcon from '@mui/icons-material/ChevronLeftOutlined'
import ChevronRightOutlinedIcon from '@mui/icons-material/ChevronRightOutlined'
import KeyboardArrowUpOutlinedIcon from '@mui/icons-material/KeyboardArrowUpOutlined'
import KeyboardArrowDownOutlinedIcon from '@mui/icons-material/KeyboardArrowDownOutlined'
import { useContext, useEffect, useRef } from 'react'
import { Box, Checkbox, FormControl, FormControlLabel, FormGroup, FormLabel, IconButton } from '@mui/material'
import { AmbientLight, DirectionalLight, Scene } from 'three'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { ThreeJSOverlayView } from '@googlemaps/three'
import { MapContext } from './MapContext'
// import { PriceMenuContext } from '../PriceMenu'
// import Modal3D from '../Modal3D'

// const AntennaItem = ({ antenna, models, scene }) => {
//   const { state, dispatch } = useContext(PriceMenuContext)
//   const { tower } = useContext(MapContext)

//   return (
//     <FormControlLabel
//       control={
//         <Checkbox
//           onChange={
//             event => {
//               const model = models.find(model => model.id === antenna.id)
//               if (event.target.checked) {
//                 scene.add(model.gltf)
//                 dispatch({
//                   type: 'update-antennas',
//                   payload: [...state.antennas, { id: antenna.id, tower: tower.name }]
//                 })
//               } else {
//                 scene.remove(model.gltf)
//                 dispatch({
//                   type: 'update-antennas',
//                   payload: state.antennas.filter(item => item.id !== antenna.id)
//                 })
//               }
//             }
//           }
//         />
//       }
//       label={antenna.height + ', ' + antenna.direction}
//     />
//   )
// }

const Map3D = () => {
  const ref = useRef(null)
  const { tower} = useContext(MapContext)

  const scene = new Scene()
  const ambientLight = new AmbientLight(0xffffff, 0.75)
  const directionalLight = new DirectionalLight(0xffffff, 0.25)
  directionalLight.position.set(0, 10, 50)
  scene.add(ambientLight)
  scene.add(directionalLight)

  const loader = new GLTFLoader()

  const models = []

  useEffect(
    () => {
      const options = {
        center: tower.center,
        zoom: 18,
        heading: 0,
        tilt: 90,
        mapId: '90f87356969d889c',
      }
      const map = new google.maps.Map(ref.current, options)

      const buttons = [
        [<ChevronLeftOutlinedIcon key='left' />, 'rotate', 20, google.maps.ControlPosition.LEFT_CENTER],
        [<ChevronRightOutlinedIcon key='right' />, 'rotate', -20, google.maps.ControlPosition.RIGHT_CENTER],
        [<KeyboardArrowDownOutlinedIcon key='down' />, 'tilt', 20, google.maps.ControlPosition.BOTTOM_CENTER],
        [<KeyboardArrowUpOutlinedIcon key='up' />, 'tilt', -20, google.maps.ControlPosition.TOP_CENTER],
      ]
      buttons.forEach(([icon, mode, amount, position]) => {
        const div = document.createElement('div')
        ReactDOM.render(
          <IconButton
            size='small'
            onClick={
              () => {
                switch (mode) {
                  case 'rotate':
                    map.setHeading(map.getHeading() + amount)
                    break
                  case 'tilt':
                    map.setTilt(map.getTilt() + amount)
                  default:
                    break
                }
              }
            }
            sx={{
              margin: '8px',
              backgroundColor: '#fff',
            }}
          >
            {icon}
          </IconButton>,
          div
        )
        map.controls[position].push(div)
      })

      console.log("tower model", tower.model)

      loader.load(tower.model, gltf => {
        gltf.scene.position.set(...tower.position)
        gltf.scene.scale.set(...tower.scale)
        scene.add(gltf.scene)
        })


      // loader.load(tower.model, gltf => {
      //   gltf.scene.position.set(...tower.position)
      //   gltf.scene.scale.set(...tower.scale)
      //   scene.add(gltf.scene)
      // })
      // antennas.forEach(antenna => {
      //   loader.load(antenna.model, gltf => {
      //     gltf.scene.scale.set(...antenna.scale)
      //     gltf.scene.position.set(...antenna.position)
      //     models.push({ id: antenna.id, gltf: gltf.scene })
      //   })
      // })

      new ThreeJSOverlayView({
        map,
        scene,
        anchor: { ...options.center, altitude: 100 },
      })
    }
  )

  return (
    <Box>
      {/* <FormControl component='fieldset' sx={{ padding: '16px' }}>
        <FormLabel component='legend'>Select slots...</FormLabel>
        <FormGroup row>
          {antennas.map(antenna => <AntennaItem key={antenna.id} antenna={antenna} models={models} scene={scene} />)}
          <Modal3D />
        </FormGroup>
      </FormControl> */}
      <Box ref={ref} sx={{ height: '70vh' }} />
    </Box>
  )
}

export default Map3D