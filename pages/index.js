import { useRef, useState } from 'react';
import { Box, Button, cardClasses, Typography } from '@mui/material';
import Grid from '@mui/material/Grid';
import styled from '@emotion/styled';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import { Wrapper } from '@googlemaps/react-wrapper';
import { Map, MapProvider } from '../components/map';
import { LoadingButton } from '@mui/lab';
import SaveIcon from '@mui/icons-material/Save';

const InputUpload = styled('input')({
  visibility: 'hidden',
  position: 'fixed',
  left: 1000
})

const BoxMap = styled('div')({
  backgroundColor: 'aqua',
  borderWidth: '1px',
  borderStyle: 'solid',
  borderRadius: '10px',
  borderColor: 'red',
  height: '90%'
})


let tower =  {
  "id": 1,
  "name": "Louvre First Tower",
  "city": "Paris",
  "district": "Louvre",
  "status": "Available",
  "image": "https://images.unsplash.com/photo-1537984827217-96d3397ba7ec",
  "model": "/models/scene.gltf",
  "position": [0, -100, 0],
  "scale": [2, 2, 2],
  "center": { "lat": 48.862326, "lng": 2.307335 },
  // "map": "https://www.google.com/maps/embed/v1/place?key=AIzaSyBs9L1nIrGJmXnPoNdyDmOwJthAr2ZMT3Q&q=8FW4V7WW%2B5Q"
}

export default function Home() {
  const [loading, setLoading] = useState(false)
  const input = useRef(null)

  const handleUpload = () => {
    setLoading(true)
    input.current.click()
    input.current.onchange = function(event) {
      console.log('begin')
      const formData = new FormData()
      formData.append('file',input.current.files[0])
      formData.append('id','1')
      formData.append('type','tower')
      console.log('form dảa', formData)
      fetch('http://146.148.69.45/api/v1/upload/model/', {
        method: 'POST',
        body: formData
      })
      .then(response => response.json())
      .then(data => {
        console.log('aaaaas')
        console.log("after",data)
        setLoading(false)
      })
      .catch(error => {
        console.error(error)
      })
      console.log('end')
    }
  }

  const handleClick = () =>{
    console.log('click3')
    console.log('on click')
    console.log('loading', loading)
  }

  return (
    <Box sx={{ margin : '30px'}}>
        <Typography variant="h4" gutterBottom component="div">
          Edit Model Site
        </Typography>

        <InputUpload ref={input} type='file'/>
        {
          loading ?
          <LoadingButton
            loading
            loadingPosition="start"
            startIcon={<SaveIcon />}
            variant="outlined"
            >
              Uploading
          </LoadingButton>
          :
          <Button variant="outlined" onClick={handleUpload}>Upload</Button>
        }
        <Box sx={{ flexGrow: 1, marginTop: 3 }}>
          <Grid container spacing={8} sx={{ height: '75vh'}}>
            <Grid item xs={8}>
              <BoxMap>
                <MapProvider tower={tower}>
                  <Map />
                </MapProvider>
              </BoxMap>
            </Grid>
            <Grid item xs={4}>
              <Grid container spacing={6}>
                <Grid item xs={12}>
                  <FormControl variant="standard">
                    <Typography variant="h6" gutterBottom component="div">
                      Scale
                    </Typography>
                    <Grid container spacing={2}>
                      <Grid item xs={3}>
                        <TextField defaultValue="1.2" id="bootstrap-input" />
                      </Grid>
                      <Grid item xs={3}>
                        <TextField defaultValue="1.2" id="bootstrap-input" />
                      </Grid>
                      <Grid item xs={3}>
                        <TextField defaultValue="1.2" id="bootstrap-input" />
                      </Grid>
                    </Grid> 
                  </FormControl>
                </Grid>
                <Grid item xs={12}>
                  <FormControl variant="standard">
                    <Typography variant="h6" gutterBottom component="div">
                      Position
                    </Typography>
                    <Grid container spacing={2}>
                      <Grid item xs={3}>
                        <TextField defaultValue="1.2" id="bootstrap-input" />
                      </Grid>
                      <Grid item xs={3}>
                        <TextField defaultValue="1.2" id="bootstrap-input" />
                      </Grid>
                      <Grid item xs={3}>
                        <TextField defaultValue="1.2" id="bootstrap-input" />
                      </Grid>
                    </Grid> 
                  </FormControl>
                </Grid>
                <Grid item xs={12}>
                  <FormControl variant="standard">
                    <Typography variant="h6" gutterBottom component="div">
                      Rotate
                    </Typography>
                    <TextField defaultValue="1.2" id="bootstrap-input" />
                  </FormControl>
                </Grid>
                <Button onClick={handleClick}>demo click 1</Button>
                
              </Grid>
            </Grid>
          </Grid>
        </Box>
    </Box>
  )
}
